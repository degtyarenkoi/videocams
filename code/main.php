﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>VideoCams</title>

<link rel="stylesheet" type="text/css" href="%root%/reset.css" />
<link rel="stylesheet" type="text/css" href="%root%/contacts.css" />
<link rel="stylesheet" type="text/css" href="%root%/slider.css" />
<link rel="stylesheet" type="text/css" href="%root%/advantages.css" />
<link rel="stylesheet" type="text/css" href="%root%/aboutus.css" />
<link rel="stylesheet" type="text/css" href="%root%/catalog.css" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="%root%/menu.js"></script>

<script type="text/javascript" src="%root%/cycle.js"></script>
<script src="%root%/slider.js"></script>

</head>

<body>
	<div id="up">
		<img src="../imgs/up.png" height="50" />
	</div>
<!--Шапка-->

	<div id="header">	
			<div id="rightMenu">
				<ul>
					<a href="#"><li>Головна</li></a>
					<a href="#"><li>Категорії обладнання</li></a>
					<a href="#"><li>Про нас</li></a>
				</ul>
			</div>	
		<div id="contacts">
			<div id="div1">УКРАЇНСЬКІ СИСТЕМИ<br><span>ВІДЕОСПОСТЕРЕЖЕННЯ</span></div>
			<form method="GET">
				<input type="hidden" name="option" value="search" />
				<input type="text" name="q" value="" />
				<input type="submit" value="Пошук" />
			</form>
			<div style="display: inline-block; margin-left: 370px">
				<a href="?option=auth">Вхід | </a>
				<a href="?option=register">Реєстрація</a>
			</div>
			<div id="div2">
				<p>tel: +38(000)000-00-00</p>
				<p>e-mail: videocams@gmail.com</p>
				<p>skype: videoCams</p>
			</div>
		</div>
		<div id="menu">		
			<ul>
				<li><a href="#"><img id="img1" src="../imgs/logo.gif" height="50" /></a></li>
				<li><a href="?option=index"><img id="img2" src="../imgs/home.png" height="50" /></a></li>	
				<a href="?option=cameras"><li id="1">КАМЕРИ
					<div id="drop_menu1"> 
						<p><a href="#"> * Провідні</a></p>
						<p><a href="#"> * Беспровідні</a></p>
					</div>
				</li></a>
				<li id="2">РЕГІСТРАТОРИ
					<div id="drop_menu2">
						<p><a href="#"> * DVR</a></p>
						<p><a href="#"> * NVR</a></p>
					</div>
				</li>
				<li id="3">КОМУТАТОРИ
					<div id="drop_menu3">
						<p><a href="#"> * Cisco</a></p>
						<p><a href="#"> * D-Link</a></p>
					</div>
				</li>
				<li id="4">КРІПЛЕННЯ
					<div id="drop_menu4">
						<p><a href="#"> * Настінні</a></p>
						<p><a href="#"> * На стелю</a></p>
					</div>
				</li>
				<li><a href="#">ПОСЛУГИ</a></li>
				<li id="div3">
					<img id="mB" src="../imgs/mB.png" height="40" />
				</li>
			</ul>
		</div>
	</div>
<!--Слайдер-->
<div id="slider">
	<div class="back"></div>
	<div class="images">
		<img src="../imgs/s1.jpg" width="1349" height="530">
		<img src="../imgs/s2.jpg" width="1349" height="530">
		<img src="../imgs/s3.jpg" width="1349" height="530">
	</div>
	<div class="next"></div>
</div>
<!--О нас-->
	<div id="about">
		<p id="head">Коло професіоналів</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	</div>
<!--Почему стоит работать с нами-->
	<div id="advantages">
		<p id="head">Чому з нами варто працювати</p>
		<div class="adv">
			<img  src="../imgs/q1.jpg" height="100" width="170"/>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
		<div class="adv">
			<img  src="../imgs/q2.png" height="100" width="150"/>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
		<div class="adv">
			<img  src="../imgs/q3.png" height="90" />
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
	</div>
<!--Найбільший попит-->
	<div id="catalog">
		%content%		
	</div>
	<div id="nav">
		<!--<a href="#">|<</a>
		<a href="#"><</a>
		<a href="#">1</a>
		<a href="#">2</a>
		<a href="#">3</a>
		<a href="#">></a>
		<a href="#">>|</a>-->
	</div>
<!--FOOTER-->
	<div id="footer">
		<div id="full_lst">
			<div class="lst">
				<ul>
					<li><a href="#">КАМЕРИ</a></li>
					<li><a href="#">Провідні</a></li>
					<li><a href="#">Беспровідні</a></li>
				</ul>
			</div>
			<div class="lst">
				<ul>
					<li><a href="#">РЕГІСТРАТОРИ</a></li>
					<li><a href="#">DVR</a></li>
					<li><a href="#">NVR</a></li>
				</ul>
			</div>
			<div class="lst">
				<ul>
					<li><a href="#">КОМУТАТОРИ</a></li>
					<li><a href="#">Cisco</a></li>
					<li><a href="#">D-Link</a></li>
				</ul>
			</div>
			<div class="lst">
				<ul>
					<li><a href="#">КРІПЛЕННЯ</a></li>
					<li><a href="#">Настінні</a></li>
					<li><a href="#">На стелю</a></li>
				</ul>
			</div>
			<div class="lst">
				<ul>
					<li><a href="#">ПОСЛУГИ</a></li>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>
