<?php
	class db {
		private $_db;
		public static $mysqli = null;
		
		private function __construct() {
			$ob_mysqli = @new mysqli(
								"localhost",
								"admin",
								"admin",
								"videocams"
								);
			if(!$ob_mysqli -> connect_error) {
				$this->_db = $ob_mysqli;
				return $this->_db;
			}
			else 
				throw new Exception("Нет подключения к БД!");
		}
		
		public static function getObject() {
			if(self::$mysqli == null) {
				$obj = new db();
				self::$mysqli = $obj; // ... = $obj -> _db;
			}
			return self::$mysqli;
		}
		
		public function query($sql) {
			return $this->_db->query($sql);
		}
		
		public function fetch_assoc($result) {
			return $result->fetch_assoc();
		}
	
	}
	
	
?>