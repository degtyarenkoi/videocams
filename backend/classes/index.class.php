<?php
	class Index extends Core {
		function getContent() {
			//global $mysqli;
			
			$result_content = "";
			$limit = QUANTITY_ITEMS;
			$to = 0;
			
			$page = 1;
			if(isset($_GET['page'])) {
				$num_page = $this->toInteger($_GET['page']);
				$page = $num_page;
			}
			
			$to = $page*$limit - $limit;
			
			$mysqli = db::getObject();
			
			$quantity_all_cameras = $mysqli->query("SELECT * FROM cameras");
			$quantity_all_cameras = $quantity_all_cameras->num_rows;
						
			$result = $mysqli->query("SELECT * FROM cameras LIMIT {$to}, {$limit}");
			$rows = $mysqli->fetch_assoc($result);
			$result_content .= $this->pageNav($page, $quantity_all_cameras);
			do{
				$result_content .= $this->getView(
											"tmp_url", $rows['image'],
											"tmp_title", $rows['title'],
											"tmp_kind", $rows['kind'],
											"tmp_resolution", $rows['resolution'],
											"tmp_night", $rows['night'],
											"tmp_id", $rows['id']
												);
				/*$result_content .= "<div id='product'>";
					$result_content .= "<img src='".$rows['image']."' height='100' />";
					$result_content .= "<p><b>IP-камера:".$rows['title']."</b></p>";
					$result_content .= "<p><b>Вид: ".$rows['kind']."</b></p>";
					$result_content .= "<p><b>Возможность ночной съемки:".$rows['night']."</b></p>";
					$result_content .= "<p><b>Разрешение видео:".$rows['resolution']."</b></p>";
				$result_content .= "</div>"; */
			}
			while($rows = $mysqli->fetch_assoc($result));	
			
			$result_content .= $this->pageNav($page, $quantity_all_cameras);

			return $result_content;
		}
	}
	
	
?>