<?php
	abstract class Core {
		public function __construct() {
			session_start();
		}
	
		public function getBody() {
			$path = ROOT."/../code/main.php";
			if(file_exists($path)) {
				$tmp = file_get_contents($path);
				$tmp = $this -> replaceStringContent($tmp);
				$tmp = $this -> replaceStringRoot($tmp);
				echo $tmp;
			}	
			else throw new Exception ("Нет шаблона для отображения!");
		}
		
		
		public function replaceStringRoot($tmp) {
			$root = "cssjs";
			return str_replace("%root%", $root, $tmp);
		}
		public function replaceStringContent($tmp) {
			return str_replace("%content%", $this->getContent(), $tmp);
		}
		
		public function toInteger($param) {
			$num_page = abs((int)$param);
			return $num_page;
		}
		
		public function pageNav($nPage, $quantity) {
			// < | back | 1 | 2 | {3} | 4 | 5 | forward | >
			$first = "";
			$back = "";
			$page2left = "";
			$page1left = "";
			$page = "<a style='background:#0066FF; color:#fff;' href=''.$uri.'page='.$nPage.'' id='c'>" .$nPage. "</a>";
			$page1right = "";
			$page2right = "";
			$forward = "";
			$last = "";
			
			$add_class = "<div id='nav'>";
			$end_class = "</div>";
			
			$limit = QUANTITY_ITEMS;
			$pages = ceil($quantity / $limit);
			
			/*return $_SERVER['QUERY_STRING']; 
			option=index&page=2*/
			$uri = "?";
			foreach($_GET as $key => $value){
				$uri .= "{$key}={$value}&";
			}
			
			if($nPage > 3){
				$first = '<a href="'.$uri.'page=1">&lt</a>';
			}
			if($nPage > 1){
				$back = '<a href="'.$uri.'page='.($nPage - 1).'">&laquo</a>';
			}
			if(($nPage - 2) > 0){
				$page2left = '<a href="'.$uri.'page='.($nPage - 2).'">' .($nPage - 2). '</a>';
			}
			if(($nPage - 1) > 0){
				$page1left = '<a href="'.$uri.'page='.($nPage - 1).'">' .($nPage - 1). '</a>';
			}
			if($nPage < $pages){
				$page1right = '<a href="'.$uri.'page='.($nPage + 1).'">' .($nPage + 1). '</a>';
			}
			if(($nPage + 1) < $pages){
				$page2right = '<a href="'.$uri.'page='.($nPage + 2).'">' .($nPage + 2). '</a>';
			}
			if($nPage < $pages){
				$forward = '<a href="'.$uri.'page='.($nPage + 1).'">&raquo</a>';
			}
			if($nPage < ($pages - 2)){
				$last = '<a href="'.$uri.'page='.($pages).'">&gt</a>';
			}
						
			return $add_class.$first.$back.$page2left.$page1left.$page.$page1right.$page2right.$forward.$last.$end_class;
		}
		
		/*
		"tmp_url", $rows['image'],
		"tmp_title", $rows['title'],
		"tmp_kind", $rows['kind'],
		"tmp_resolution", $rows['resolution'],
		"tmp_night", $rows['night']
		*/
		
		
		public function getView() {
			$view = ROOT."/../code/content.php";
		
			$args = func_get_args();
			$list = array(); 
			for($i=0; $i<count($args); $i++) {
				if(!($i%2)) {
					$list[$args[$i]] = $args[$i+1];
				}
			}
			
			if(file_exists($view)) {
				$view_tmp = file_get_contents($view);
				
				foreach($list as $key => $value){
					$view_tmp = str_replace("%".$key."%", $value, $view_tmp);
				}
				return $view_tmp;
			}
			else 
				throw new Exception("Нет основого контента!");
		}
		
		
		abstract function getContent();
		
		public function clearString($str) {
			return trim(htmlspecialchars(stripslashes($str)));
		}
		
	}
	
?>