//выпадающее меню
$(document).ready(function() {
	$("li").mouseenter(function(event) {
		var i = event.target.id; 
		var d = "#drop_menu" + i; 
		$(d).slideToggle();
	});
	$("li").mouseleave(function(event) {
		var i = event.target.id; 
		var d = "#drop_menu" + i; 
		$(d).hide();
		
		$(d).mouseleave(function(event) {
			$(d).hide();
		});
	});
	
//фиксирование меню при прокрутке страницы	
	$(window).scroll(function() {
		var h_contacts = 60;
		var top = $(this).scrollTop();
		var elem = $("#menu");
		
		if(top > h_contacts) {
			elem.css({
				"position": "fixed",
				"top":"0",
				"background-color": "rgba(3,149,189, 0.7)"
			});
		}
		else {
			elem.css({
				"top":(h_contacts - top),
				"background-color": "rgba(3,149,189, 1)"
			});
		}
		//Кнопка Наверх страницы
		var up = $("#up");
		if($(window).scrollTop() > 600) 
			up.fadeIn();
		else
			up.fadeOut();
		up.click(function(){
			window.scroll(0,0);
		});
		
		/*if ($(window).scrollTop() > 64) {
			$("#menu").css({
				"position": "fixed",
				"padding": "0",
				"background-color": "rgba(3,149,189, 0.7)"
			});
			$("#contacts").hide();
		}
		else {
			$("#menu").css({
				"position": "static",
				"padding": "0",
				"background-color": "rgba(3,149,189, 1)"
			});
			$("#contacts").show();
		}*/
	});
//выпадающее меню справа
	$("#mB").click(function() {
		$("#rightMenu").toggle('slow');
		
	});

});

