<?php
	$dirname = str_replace("\\", "/", dirname(__FILE__));
	define("ROOT", $dirname);
	define("QUANTITY_ITEMS", 2);
	
	require_once ROOT."/classes/core.class.php";
	require_once ROOT."/classes/db.class.php";
	
	try {
		$option = "index";
		if(isset($_GET['option'])) {
			$opt = $_GET['option'];
			$path = ROOT."/classes/".$opt.".class.php";
			if(file_exists($path)) {
				require_once $path;
				
				if(class_exists($opt) && $opt!="core" && $opt!="db") {
					$option = $opt;	
				}
			}
		}
		require_once (ROOT."/classes/".$option.".class.php");
		
		$view = new $option();
		$view -> getBody();
		
	}
	catch(Exception $e) {
		echo $e -> getMessage();
	}
?>